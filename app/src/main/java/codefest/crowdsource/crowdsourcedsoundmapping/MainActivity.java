package codefest.crowdsource.crowdsourcedsoundmapping;

import android.app.PendingIntent;
import android.content.Intent;
import android.app.AlarmManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

import codefest.crowdsource.crowdsourcedsoundmapping.data.LocationDataObject;
import codefest.crowdsource.crowdsourcedsoundmapping.data.Factory;
import codefest.crowdsource.crowdsourcedsoundmapping.data.LocationReader;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {
    GoogleMap map = null;
    private Button btn, aboutPageBtn, testbtn;

    private AlarmManager am;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(Factory.location == null) {
            Factory.location = new LocationReader(this);
        }
        if(!Factory.location.canGetLocation())
        {
//            Factory.location.showSettingsAlert();
        }

        btn = (Button) findViewById(R.id.button);
        aboutPageBtn = (Button) findViewById(R.id.AboutPage);
        //btn.setText("DBTest");

        //testbtn = (Button) findViewById(R.id.btn_test);
        //testbtn.setOnClickListener(this);
        btn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                LaunchTestActivity();
            }
        });

        aboutPageBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                LaunchAboutActivity();
            }
        });
		
        startRepeater();
    }

    public void startRepeater() {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pintent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUESTCODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long now = System.currentTimeMillis();
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC, now, 1500, pintent);
    }

    private void LaunchAboutActivity(){
        Intent i = new Intent(this,AboutPage.class);
        startActivity(i);
    }

    public void onMapReady(GoogleMap map){
        this.map = map;
        map.getMapType();
        Log.i("IDR",""+map.isMyLocationEnabled());
        LocationReader datObj = new LocationReader(getApplicationContext());
        //LatLng loc = new LatLng(datObj.getLatitude(), datObj.getLongitude());
        LatLng loc = new LatLng(-33.87365, 151.20689);
        //map.addMarker(new MarkerOptions().position(loc).title("Marker"));
        CircleOptions co = new CircleOptions();
        co.center(loc);
        co.radius(50); //TODO Change to real values calculated on stored data.
        co.fillColor(Color.CYAN);
        double ratingQuality = 5;
        if(ratingQuality <= 2) co.strokeColor(Color.RED);
        else if (ratingQuality > 2 && ratingQuality < 3.5) co.strokeColor(Color.YELLOW);
        else if (ratingQuality >= 3.5) co.strokeColor(Color.GREEN);
        map.addCircle(co);
        changeCamera(CameraUpdateFactory.newCameraPosition(SYDNEY),null);
    }


    private void LaunchTestActivity(){
        Intent i = new Intent(getApplicationContext(), TestActivity.class);
        startActivity(i);
    }
    public void scheduleDataRead()
    {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pintent = PendingIntent.getBroadcast(this , AlarmReceiver.REQUESTCODE, intent, PendingIntent.FLAG_UPDATE_CURRENT );
        long firstTime = System.currentTimeMillis();
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC, firstTime, 5000, pintent);
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            /*case R.id.btn_test:
                LaunchTestActivity();
                break;*/
            case R.id.AboutPage:
                LaunchAboutActivity();
                break;


        }


    }
    private void changeCamera(CameraUpdate update, GoogleMap.CancelableCallback callback){
        map.moveCamera(update);
    }
    static final CameraPosition SYDNEY = new CameraPosition.Builder().target(new LatLng(-33.87365, 151.20689))
            .zoom(16.5f)
            .bearing(0)
            .tilt(0)
            .build();
}
