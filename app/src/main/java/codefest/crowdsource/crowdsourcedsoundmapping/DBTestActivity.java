
package codefest.crowdsource.crowdsourcedsoundmapping;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import codefest.crowdsource.crowdsourcedsoundmapping.types.DataBlob;

public class DBTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dbtest);

        Button run = (Button)findViewById(R.id.run);
        run.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                DBHandler x = new DBHandler(getApplicationContext());
                //x.importData("{\"decibels\":\"32\",\"Longitude\":\"30.2461\",\"Latitude\":\"-87.296\",\"timestamp\":\"16786100\"}");
                DataBlob dat = new DataBlob(32,-87.292,30.2461,23,14,16786100);
                x.importData(dat);
                x.commitData();
            }
        });
    }
}

