package codefest.crowdsource.crowdsourcedsoundmapping.data;

import android.preference.Preference;
import android.provider.CalendarContract;

/**
 * Created by Robert on 4/8/2017.
 */

public class Settings  {

    private Settings(){
    };

    // How much data we are allowed to collect.
    public static boolean allowCollection = false;
    public static boolean allowMonitorLocation = false;

    // Data collection interval.
    public static int collectionTiming = 15;

    // Personal preference settings.
    public static String noiseDesired = "some";
    public static boolean awakeDay = true;// Therefore not night
}
